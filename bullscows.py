import random
import discord
import datetime
import urllib.request
import json

from discord.ext import commands

command_prefix = 'kar?'
BOT_TOKEN = '<discord bot token here>'
bot = discord.Client()
game_state = {
    'last_check_date' : datetime.datetime(2010, 1, 1),
    'json_ip_info' : {},
    'is_playing' : False,
    'attempts' : 1 
}

def get_ip_info():
    if game_state['last_check_date'] + datetime.timedelta(minutes=10) < datetime.datetime.now():
        resp = urllib.request.urlopen("http://ip-api.com/json").read()
        if type(resp) == bytes:
            resp = resp.decode('utf-8')
        game_state['json_ip_info'] = json.loads(resp) 
    return game_state['json_ip_info']

def compare_numbers(playerNum, computerNum):
    cows = bulls = 0
    bulls = sum([1 for i in range(4) if computerNum[i] == playerNum[i]])
    cows = sum([1 for i in range(4) for j in range(4) if i != j and computerNum[i] == playerNum[j]])    
    return (cows, bulls)  

@bot.event
async def on_ready():
    print('Бот готов')
    await bot.change_presence(game=discord.Game(name='хочет играть', type=0))

@bot.event
async def on_message(message):
    if message.author.bot:
        return

    def make_a_number():
        while True:
            num = random.randrange(1000, 9999)
            if (not check_number(str(num))):
                continue
            else:
                break
        return str(num)

    def check_number(num_): 
        return num_.isdigit() and len(num_) == 4 and len(list(num_)) == len(list(set(list(num_))))

    def check(msg):
        return check_number(msg.content)

    # COMMANDS
    if message.content.startswith(command_prefix):
        com_message = message.content[len(command_prefix):].upper()
        if com_message == 'PLAY':
            if game_state['is_playing']:
                await bot.send_message(message.channel, "А я уже играю прямо сейчас")
                return

            conceived_number = make_a_number()
            game_state['is_playing'] = True
            await bot.send_message(message.channel, "Хорошо, давайте играть! Я загадал число..")
            while True:
                answer = await bot.wait_for_message(channel=message.channel, timeout=120, check=check)
                if answer == None:
                    await bot.send_message(message.channel, "Ну и молчите..")
                    game_state['is_playing'] = False
                    break
                cows, bulls = compare_numbers(answer.content, conceived_number)
                if bulls == 4:
                    await bot.send_message(message.channel, "Да! <@%s> отгадал число!\nПопыток: %d" % (answer.author.id, game_state['attempts']))
                    game_state['is_playing'] = False
                    game_state['attempts'] = 1
                    break
                else:
                    await bot.send_message(message.channel, "Хмм.. %s, здесь %d коров и %d быков" % (answer.content, cows, bulls))
                    game_state['attempts'] += 1
                    
        elif com_message == "CITY":
            city = get_ip_info()
            await bot.send_message(message.channel, "Я живу в городе %s (%s) и мой провайдер %s" % (city['city'], city['country'], city['org']))
        elif com_message == "HELP":
            await bot.send_message(message.channel, "Все очень просто! Мои команды:\n\
                **kar?city** - узнать, откуда я запущен сейчас\n\
                **kar?play** - поиграть в игру 'Быки и коровы'\n\
                **kar?playhelp** - правила игры 'Быки и коровы'")
        elif com_message == "PLAYHELP":
            await bot.send_message(message.channel, "Я задумываю тайное 4-значное число с неповторяющимися цифрами. Игрок делает первую попытку отгадать число. Попытка — это 4-значное число с неповторяющимися цифрами, сообщаемое мне! Я сообщаю в ответ, сколько цифр угадано без совпадения с их позициями в тайном числе (то есть количество коров) и сколько угадано вплоть до позиции в тайном числе (то есть количество быков)\nТвоя задача определить тайное число, используя информацию о быках и коровах")

bot.run(BOT_TOKEN)
